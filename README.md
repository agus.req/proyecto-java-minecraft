Proyecto para practicar Java. Año 2021 \
Se realiza un plugin para minecraft con diferentes funcionalidades.
- One percent world: el mundo se genera de forma aleatoria en chunks normales o vacios, generando de esta forma "islas" las cuales hay que cruzar
- Linked inventories: conecta los inventarios de todos los jugadores, por lo cual todos comparten, ven y utilizan los mismos items
- Hole in pocket: cada X cantidad de segundos por culpa de un hueco en el inventario, al jugador se le caen items
- One slot inventory: El inventario del jugador se ve reducido a un solo slot, lo cual hace que sea muy dificil de jugar
