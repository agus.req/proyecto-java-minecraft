package me.wence.test.onepercent;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

import java.util.Random;



public class OnePercent implements Listener {
    //static int firstTimeEnd=1;
    //static int[][] isLoaded = new int[18][18];
    @EventHandler
    public void onePercentWorld(ChunkLoadEvent evento){
        Chunk chunky = evento.getChunk();
        int NoGen=3000;
        int posX = chunky.getX();
        int posZ = chunky.getZ();
        if (evento.getChunk().isLoaded() && evento.getWorld().isChunkGenerated(posX, posZ) && evento.isNewChunk()) {
            Random r = new Random();
            int min = 0;
            int max = 90;
            int rand = r.nextInt((max - min) + 1) + min;
            if (rand != 3) {
                int minX = posX * 16;
                int maxX = minX + 15;
                int maxY = chunky.getWorld().getMaxHeight();
                int minZ = posZ * 16;
                int maxZ = minZ + 15;

                int nope = 0;
                for (int x = minX; x <= maxX; ++x) {
                    for (int y = 0; y <= maxY; ++y) {
                        for (int z = minZ; z <= maxZ; ++z) {
                            if (evento.getWorld().getBlockAt(x, y, z).getType() == Material.END_PORTAL_FRAME)
                                nope = nope + 1;
                            if (evento.getWorld().getName().endsWith("_nether") && (evento.getWorld().getBlockAt(x, y, z).getType() == Material.SPAWNER || evento.getWorld().getBlockAt(x, y, z).getType() == Material.OBSIDIAN))
                                nope = nope + 1;
                            if (x == NoGen && z == NoGen)
                                nope = nope + 1;
                        }
                    }
                }

                if (nope == 0) {
                    for (int x = minX; x <= maxX; ++x) {
                        for (int y = 0; y <= maxY; ++y) {
                            for (int z = minZ; z <= maxZ; ++z) {
                                if (!(evento.getWorld().getBlockAt(x, y, z).getType() == Material.AIR))
                                    evento.getWorld().getBlockAt(x, y, z).setType(Material.AIR, false);
                            }
                        }
                    }
                }

            }
        }
    }
}

