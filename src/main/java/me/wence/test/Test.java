package me.wence.test;


import me.wence.test.commands.Comandos;
import me.wence.test.holeinpocket.Holeinpocket;
import me.wence.test.linkedinventories.LinkedInventories;
import me.wence.test.onepercent.OnePercent;
import me.wence.test.oneslotinv.OneSlotInv;
import org.bukkit.block.Biome;
import org.bukkit.plugin.java.JavaPlugin;

public final class Test extends JavaPlugin {
    public static Test plugin;

    /*public ChunkGenerator getDefaultWorldGenerator(String worldName, String id)
    {
        return new OnePercentGen();
    }*/

    @Override
    public void onEnable() {
        plugin=this;
        System.out.println("Holis");


        //WorldSpawn
        int NoGen = 3000;
        getServer().getWorld("world").setSpawnLocation(NoGen,70,NoGen);
        for(int x = NoGen; x < NoGen+15; x++) {
            for(int z = NoGen; z < NoGen+15; z++) {
                for(int y=0; y< getServer().getWorld("world").getMaxHeight(); y++)
                    getServer().getWorld("world").setBiome(x,y,z, Biome.FOREST);
            }
        }

        //One Percent World
        OnePercent onep = new OnePercent();
        getServer().getPluginManager().registerEvents(onep, this);

        //Linked inventories
        LinkedInventories linkedinv = new LinkedInventories();
        getServer().getPluginManager().registerEvents(linkedinv, this);
        getCommand("linkedinv").setExecutor(linkedinv);

        //Commands
        Comandos comms = new Comandos();
        getCommand("pos").setExecutor(comms);
        //getCommand("genOver").setExecutor(comms);
        //getCommand("genNether").setExecutor(comms);
        getCommand("genEnd").setExecutor(comms);

        //Hole in Pocket
        Holeinpocket hole = new Holeinpocket();
        getServer().getPluginManager().registerEvents(hole, this);
        getCommand("hole").setExecutor(hole);

        //One slot inventories
        OneSlotInv oneslotinv = new OneSlotInv();
        getServer().getPluginManager().registerEvents(oneslotinv, this);
        getCommand("oneslotinv").setExecutor(oneslotinv);
    }



    @Override
    public void onDisable() {
        plugin=null;
        // Plugin shutdown logic
    }



}
