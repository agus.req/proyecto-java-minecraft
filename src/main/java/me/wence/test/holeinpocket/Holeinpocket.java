package me.wence.test.holeinpocket;

import me.wence.test.Test;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import java.util.Random;


public class Holeinpocket implements Listener, CommandExecutor {

    //private ArrayList<UUID> hasHoles = new ArrayList<UUID>();
    static int delayHoles=10;
    static int schedulerid=-1;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("hole")) {
            if (args.length < 1){
                sender.sendMessage(ChatColor.RED+"Wrong instance.");
                //sender.sendMessage(ChatColor.RED+"/hole <player>");
                sender.sendMessage(ChatColor.RED+"/hole <delay in seconds>");
            }
            else{
                if(Integer.parseInt(args[0])>0) {
                    delayHoles=Integer.parseInt(args[0]);
                    if (schedulerid >= 0) {
                        Bukkit.getServer().getScheduler().cancelTask(schedulerid);
                        schedulerid=-1;
                    }
                    schedulerid = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Test.plugin, () -> {
                        Random r = new Random();
                        int min = 0;
                        int max = 35;
                        int rand = r.nextInt((max - min) + 1) + min;
                        for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                            onlinePlayers.getInventory().setItem(rand, null);
                            onlinePlayers.updateInventory();
                        }
                    }, 1, 20 * delayHoles);
                    if (delayHoles<60)
                        Bukkit.broadcastMessage(ChatColor.GREEN+"Everyone has a hole in their pocket! You will lose an item from a random slot each "+delayHoles+" seconds!");
                    else
                        Bukkit.broadcastMessage(ChatColor.GREEN+"Everyone has a hole in their pocket! You will lose an item from a random slot each "+delayHoles/60+" minutes and "+(delayHoles-60*((int)delayHoles/60))+" seconds!");
                }
                else if(Integer.parseInt(args[0])==0) {
                    if (schedulerid >= 0) {
                        Bukkit.broadcastMessage(ChatColor.BLUE + "There is no more holes in anyone's pockets");
                        Bukkit.getServer().getScheduler().cancelTask(schedulerid);
                        schedulerid=-1;
                    }
                    else
                        sender.sendMessage(ChatColor.RED + "There were no holes in anyone's pockets");
                }
                else
                    sender.sendMessage(ChatColor.RED+"Set a value greater than or equals to 0");
                //UUID pid = p.getUniqueId();
                //for(UUID id : hasHoles){}

            }
        }
        return true;
    }
}
