package me.wence.test.oneslotinv;

import me.wence.test.Test;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

public class OneSlotInv implements Listener, CommandExecutor {
    static int oneslotinv = 0;
    ItemStack barrier = new ItemStack(Material.BARRIER,1);
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {


        if (cmd.getName().equalsIgnoreCase("oneslotinv")) {
            if (args.length < 1){
                sender.sendMessage(ChatColor.RED+"Wrong instance.");
                sender.sendMessage(ChatColor.RED+"/oneslotinv <true/false>");
            }
            else{
                if(args[0].equalsIgnoreCase("true")){
                    Bukkit.broadcastMessage(ChatColor.GREEN+"You now have only one slot!");
                    for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                        for(int i=0; i<=35; i++) {
                            if (i == 0)
                                onlinePlayers.getInventory().setItem(40, barrier);
                            else
                                onlinePlayers.getInventory().setItem(i, barrier);
                        }
                        onlinePlayers.updateInventory();
                    }
                    oneslotinv=1;
                }
                else if(args[0].equalsIgnoreCase("false")){
                    Bukkit.broadcastMessage(ChatColor.BLUE+"Your inventory has increased it's size!");
                    for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                        for(int i=0; i<=35; i++) {
                            if (i == 0)
                                onlinePlayers.getInventory().setItem(40, null);
                            else
                                onlinePlayers.getInventory().setItem(i, null);
                        }
                        onlinePlayers.updateInventory();
                    }
                    oneslotinv=0;
                }
                else{
                    sender.sendMessage(ChatColor.RED+"Wrong instance.");
                    sender.sendMessage(ChatColor.RED+"/oneslotinv <true/false>");
                }
            }
        }
        return true;
    }

    /*@EventHandler
    public void onEntityPickupItem(EntityPickupItemEvent e) {
        if (oneslotinv==1) {

        }
    }*/
    /*@EventHandler
    public void onPlayerDeath(EntityDeathEvent event){
        if (oneslotinv==1) {
            if (event.getEntity() instanceof Player) {
                event.getDrops().clear();
                for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                    onlinePlayers.getInventory().clear();
                    onlinePlayers.getOpenInventory().getTopInventory().clear();
                    onlinePlayers.updateInventory();
                }
            }
        }
    }*/

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (oneslotinv==1) {
            if(event.getItemDrop().getName().equalsIgnoreCase("barrier"))
                event.setCancelled(true);
        }
    }

    /*@EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        ItemStack[] inv = event.getPlayer().getInventory().getContents();
        for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
            if(onlinePlayers!=event.getPlayer()) {
                onlinePlayers.getInventory().setContents(inv);
                onlinePlayers.updateInventory();
            }
        }
    }*/

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (oneslotinv==1) {
            //event.getWhoClicked().sendMessage(event.getAction().name());
            if(!(event.getAction().equals(InventoryAction.DROP_ONE_CURSOR)||event.getAction().equals(InventoryAction.DROP_ALL_CURSOR)||event.getAction().equals(InventoryAction.NOTHING))) {
                if (event.getWhoClicked().getInventory().getItem(event.getSlot()) != null) {
                    ItemStack itemito = event.getWhoClicked().getInventory().getItem(event.getSlot());
                    if (!(event.getClickedInventory().getType().getDefaultTitle().equalsIgnoreCase("crafting"))) {
                        if (event.getSlot() == 0) {
                            Bukkit.getServer().getScheduler().runTaskLater(Test.plugin, () -> {
                                if (event.getWhoClicked().getInventory().getItem(event.getSlot()) != null) {
                                    if (event.getWhoClicked().getInventory().getItem(event.getSlot()).getType().name().equalsIgnoreCase("barrier")) {
                                        Player pla = (Player) event.getWhoClicked();
                                        for (int i = 0; i <= 35; i++) {
                                            if (i == 0)
                                                pla.getInventory().setItem(40, barrier);
                                            else
                                                pla.getInventory().setItem(i, barrier);
                                        }
                                        pla.getInventory().setItem(event.getSlot(), itemito);
                                        pla.updateInventory();
                                    }
                                }
                            }, 1);
                        } else if (event.getSlot() > 0 && event.getSlot() < 41) {
                            event.setResult(Event.Result.DENY);
                            event.setCancelled(true);
                            Bukkit.getServer().getScheduler().runTaskLater(Test.plugin, () -> {
                                Player pla = (Player) event.getWhoClicked();
                                for (int i = 0; i <= 35; i++) {
                                    if (i == 0)
                                        pla.getInventory().setItem(40, barrier);
                                    else
                                        pla.getInventory().setItem(i, barrier);
                                }
                                pla.updateInventory();
                                if (event.getCurrentItem() == barrier) {
                                    pla.setItemOnCursor(null);
                                    event.setCurrentItem(null);
                                }
                            }, 1);
                        }
                    } else {
                        Bukkit.getServer().getScheduler().runTaskLater(Test.plugin, () -> {
                            Player pla = (Player) event.getWhoClicked();
                            if(pla.getOpenInventory().getTopInventory().getItem(event.getSlot())!=null) {
                                if (pla.getOpenInventory().getTopInventory().getItem(event.getSlot()).getType() == Material.BARRIER) {
                                    for (int j = 0; j <= 35; j++) {
                                        if (j == 0)
                                            pla.getInventory().setItem(40, barrier);
                                        else
                                            pla.getInventory().setItem(j, barrier);
                                    }
                                    pla.getOpenInventory().getTopInventory().setItem(event.getSlot(), null);
                                }
                                pla.updateInventory();
                                if (event.getCurrentItem() == barrier) {
                                    pla.setItemOnCursor(null);
                                    event.setCurrentItem(null);
                                }
                            }
                        }, 1);
                    }
                }
            }
        }
    }

    /*@EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        if (oneslotinv==1) {
            if(event.getCursor().getType().equals(Material.BARRIER)){

                Player pla = (Player) event.getWhoClicked();
                for (int i = 0; i <= 35; i++) {
                    if (i == 0)
                        pla.getInventory().setItem(40, barrier);
                    else
                        pla.getInventory().setItem(i, barrier);
                }
                event.setResult(Event.Result.DENY);
                pla.sendMessage(event.getResult().name());
                pla.updateInventory();
                event.setCursor(null);
                pla.setItemOnCursor(null);
                event.setCancelled(true);
            }
        }
    }*/

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (oneslotinv==1) {
            if(event.getBlockPlaced().getType().equals(Material.BARRIER)){
                for (int i = 0; i <= 8; i++) {
                    if(i==0)
                        event.getPlayer().getInventory().setItem(40, barrier);
                    else
                        event.getPlayer().getInventory().setItem(i, barrier);
                }
                event.setCancelled(true);
            }
        }
    }
}
