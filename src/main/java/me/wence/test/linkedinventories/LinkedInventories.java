package me.wence.test.linkedinventories;

import me.wence.test.Test;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;

public class LinkedInventories implements Listener, CommandExecutor {
    static int linkedinv = 0;
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {


        if (cmd.getName().equalsIgnoreCase("linkedinv")) {
            if (args.length < 1){
                sender.sendMessage(ChatColor.RED+"Wrong instance.");
                sender.sendMessage(ChatColor.RED+"/linkedinv <true/false>");
            }
            else{
                if(args[0].equalsIgnoreCase("true")){
                    Bukkit.broadcastMessage(ChatColor.GREEN+"Inventories Linked.");
                    linkedinv=1;
                }
                else if(args[0].equalsIgnoreCase("false")){
                    Bukkit.broadcastMessage(ChatColor.BLUE+"Inventories Unlinked.");
                    linkedinv=0;
                }
                else{
                    sender.sendMessage(ChatColor.RED+"Wrong instance.");
                    sender.sendMessage(ChatColor.RED+"/linkedinv <true/false>");
                }
            }
        }
        return true;
    }

    @EventHandler
    public void onEntityPickupItem(EntityPickupItemEvent e) {
        if (linkedinv==1) {
            ItemStack it = e.getItem().getItemStack();
            ((Player) e.getEntity()).getInventory().addItem(it);
            ItemStack[] inv = ((Player) e.getEntity()).getInventory().getContents();
            for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                onlinePlayers.getInventory().setContents(inv);
                onlinePlayers.updateInventory();
            }
            ((Player) e.getEntity()).getInventory().removeItem(it);
        }
    }
    @EventHandler
    public void onPlayerDeath(EntityDeathEvent event){
        if (linkedinv==1) {
            if (event.getEntity() instanceof Player) {
                event.getDrops().clear();
                for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                    onlinePlayers.getInventory().clear();
                    onlinePlayers.getOpenInventory().getTopInventory().clear();
                    onlinePlayers.updateInventory();
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (linkedinv==1) {
            ItemStack[] inv = event.getPlayer().getInventory().getContents();
            for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                onlinePlayers.getInventory().setContents(inv);
                onlinePlayers.updateInventory();
            }
        }
    }

    /*@EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        ItemStack[] inv = event.getPlayer().getInventory().getContents();
        for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
            if(onlinePlayers!=event.getPlayer()) {
                onlinePlayers.getInventory().setContents(inv);
                onlinePlayers.updateInventory();
            }
        }
    }*/

    @EventHandler
    public void onBucketPlace(PlayerBucketEmptyEvent event){
        if (linkedinv==1) {
            ItemStack[] inv = event.getPlayer().getInventory().getContents();
            for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                onlinePlayers.getInventory().setContents(inv);
                onlinePlayers.updateInventory();
            }
        }
    }

    @EventHandler
    public void onBucketTake(PlayerBucketFillEvent event){
        if (linkedinv==1) {
            ItemStack[] inv = event.getPlayer().getInventory().getContents();
            for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                onlinePlayers.getInventory().setContents(inv);
                onlinePlayers.updateInventory();
            }

        }
    }

    @EventHandler
    public void onEating(PlayerItemConsumeEvent event){
        if (linkedinv==1) {
            ItemStack[] inv = event.getPlayer().getInventory().getContents();
            for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                onlinePlayers.getInventory().setContents(inv);
                onlinePlayers.updateInventory();
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (linkedinv==1) {
            for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                if (event.getWhoClicked() instanceof Player) {
                    Player pla = (Player) event.getWhoClicked();
                    if (onlinePlayers != pla) {
                        Bukkit.getServer().getScheduler().runTaskLater(Test.plugin, () -> {
                            ItemStack[] inv = event.getWhoClicked().getInventory().getContents();
                            onlinePlayers.getInventory().setContents(inv);
                            onlinePlayers.updateInventory();
                        }, 1);

                    }
                }
            }
        }
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        if (linkedinv==1) {
            for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                if (event.getWhoClicked() instanceof Player) {
                    Player pla = (Player) event.getWhoClicked();
                    if (onlinePlayers != pla) {
                        Bukkit.getServer().getScheduler().runTaskLater(Test.plugin, () -> {
                            ItemStack[] inv = event.getWhoClicked().getInventory().getContents();
                            onlinePlayers.getInventory().setContents(inv);
                            onlinePlayers.updateInventory();
                        }, 1);

                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (linkedinv==1) {
            if (event.getPlayer().getGameMode() == GameMode.SURVIVAL) {
                if (event.getItemInHand().getMaxStackSize() > 1) {
                    event.getItemInHand().setAmount(event.getItemInHand().getAmount() - 1);
                    ItemStack[] inv = event.getPlayer().getInventory().getContents();
                    for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                        onlinePlayers.getInventory().setContents(inv);
                        onlinePlayers.updateInventory();
                    }
                }
            }
        }
    }
}
