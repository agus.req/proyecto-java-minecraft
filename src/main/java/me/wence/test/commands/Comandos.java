package me.wence.test.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

public class Comandos implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if(!(sender instanceof Player)){ return true; }
        Player player = (Player) sender;

        if(cmd.getName().equalsIgnoreCase("pos")){
            player.sendMessage("Posicion X: "+player.getWorld().getChunkAt(player.getLocation()).getX());
            player.sendMessage("Posicion Z: "+player.getWorld().getChunkAt(player.getLocation()).getZ());
            player.sendMessage("En el mundo: "+player.getWorld().getName());
        }

        else if(cmd.getName().equalsIgnoreCase("genEnd")){
            if(player.getName().equalsIgnoreCase("Troctor")) {
                for (int i = -9; i < 9; i++) {
                    for (int j = -9; j < 9; j++) {
                        player.sendMessage("Se genero el chunk en la posicion X:" + i + " Z:" + j);
                        int min = 0;
                        int max = 3;
                        Random r = new Random();
                        int rand = r.nextInt((max - min) + 1) + min;

                        if (rand != 1) {
                            int minX = i * 16;
                            int maxX = minX + 15;
                            int maxY = Bukkit.getServer().getWorld("world_the_end").getMaxHeight();
                            int minZ = j * 16;
                            int maxZ = minZ + 15;
                            for (int x = minX; x <= maxX; ++x) {
                                for (int y = 0; y <= maxY; ++y) {
                                    for (int z = minZ; z <= maxZ; ++z) {
                                        if (!(Bukkit.getServer().getWorld("world_the_end").getBlockAt(x, y, z).getType() == Material.AIR || Bukkit.getServer().getWorld("world_the_end").getBlockAt(x, y, z).getType() == Material.OBSIDIAN || Bukkit.getServer().getWorld("world_the_end").getBlockAt(x, y, z).getType() == Material.BEDROCK || Bukkit.getServer().getWorld("world_the_end").getBlockAt(x, y, z).getType() == Material.END_CRYSTAL || Bukkit.getServer().getWorld("world_the_end").getBlockAt(x, y, z).getType() == Material.IRON_BARS)) {
                                            Bukkit.getServer().getWorld("world_the_end").getBlockAt(x, y, z).setType(Material.AIR, true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                player.sendMessage("No tienes permiso para usar este comando uwu");
            }
        }

        /*else if(cmd.getName().equalsIgnoreCase("genOver")){
            if(player.getName().equalsIgnoreCase("Troctor")) {
                for (int i = -25; i < 25; i++) {
                    for (int j = -25; j < 25; j++) {
                        player.sendMessage("Se genero el chunk en la posicion X:" + i + " Z:" + j);
                        int min = 0;
                        int max = 90;
                        Random r = new Random();
                        int rand = r.nextInt((max - min) + 1) + min;

                        if (rand != 1) {
                            int minX = i * 16;
                            int maxX = minX + 15;
                            int maxY = Bukkit.getServer().getWorld("world").getMaxHeight();
                            int minZ = j * 16;
                            int maxZ = minZ + 15;
                            int nope=0;

                            for (int x = minX; x <= maxX; ++x) {
                                for (int y = 0; y <= maxY; ++y) {
                                    for (int z = minZ; z <= maxZ; ++z) {
                                        if (Bukkit.getServer().getWorld("world").getBlockAt(x, y, z).getType() == Material.END_PORTAL_FRAME)
                                            nope = nope + 1;
                                    }
                                }
                            }

                            if(nope==0) {
                                for (int x = minX; x <= maxX; ++x) {
                                    for (int y = 0; y <= maxY; ++y) {
                                        for (int z = minZ; z <= maxZ; ++z) {
                                            if (!(Bukkit.getServer().getWorld("world").getBlockAt(x, y, z).getType() == Material.AIR)) {
                                                Bukkit.getServer().getWorld("world").getBlockAt(x, y, z).setType(Material.AIR, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                player.sendMessage("No tienes permiso para usar este comando uwu");
            }
        }

        else if(cmd.getName().equalsIgnoreCase("genNether")){
            if(player.getName().equalsIgnoreCase("Troctor")) {
                for (int i = -15; i < 15; i++) {
                    for (int j = -15; j < 15; j++) {
                        player.sendMessage("Se genero el chunk en la posicion X:" + i + " Z:" + j);
                        int min = 0;
                        int max = 70;
                        Random r = new Random();
                        int rand = r.nextInt((max - min) + 1) + min;

                        if (rand != 1) {
                            int minX = i * 16;
                            int maxX = minX + 15;
                            int maxY = Bukkit.getServer().getWorld("world_nether").getMaxHeight();
                            int minZ = j * 16;
                            int maxZ = minZ + 15;

                            int nope=0;
                            for (int x = minX; x <= maxX; ++x) {
                                for (int y = 0; y <= maxY; ++y) {
                                    for (int z = minZ; z <= maxZ; ++z) {
                                        if ((Bukkit.getServer().getWorld("world_nether").getBlockAt(x, y, z).getType() == Material.SPAWNER))
                                            nope = nope + 1;
                                    }
                                }
                            }
                            if(nope==0) {
                                for (int x = minX; x <= maxX; ++x) {
                                    for (int y = 0; y <= maxY; ++y) {
                                        for (int z = minZ; z <= maxZ; ++z) {
                                            if (!(Bukkit.getServer().getWorld("world").getBlockAt(x, y, z).getType() == Material.AIR)) {
                                                Bukkit.getServer().getWorld("world").getBlockAt(x, y, z).setType(Material.AIR, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                player.sendMessage("No tienes permiso para usar este comando uwu");
            }
        }*/

        return true;
    }
}

